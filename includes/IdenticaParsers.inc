<?php
//$Id$
abstract class IdenticaParser {
  protected $_content = null;
  public function get($url) {
    $response = drupal_http_request($url);
    //var_dump($response, $url);
    if (!$response || ($response->code != 200 && $response->code != 304)) {
	    throw new IdenticaRequestException("Invalid request");
    }
    $this->_content = $response->data;
  }

  abstract public function toArray();
}

class IdenticaJsonParser extends IdenticaParser {
  private $_data = null;

  public function get($url) {
    $this->_data = null;
    parent::get($url);
  }

    public function toArray() {
      if (is_null($this->_content)) return array();
      if (is_null($this->_data)) {
        $data = json_decode($this->_content);
        foreach ($data->results as $post) {
          $row = array();
          $row['user'] = $post->from_user;
          $row['profile_image_url'] = $post->profile_image_url;
          $row['text'] = _identica_profile_parse_hashtags_users($post->text);
          $this->_data[] = $row;
        }
      }
      return $this->_data;
    }
}


class IdenticaRequestException extends Exception {}