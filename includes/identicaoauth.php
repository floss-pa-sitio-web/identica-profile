<?php

require_once drupal_get_path('module', 'identica_profile') . '/identica_profile.inc';
require_once 'twitteroauth.php';
class IdenticaOAuth extends TwitterOAuth {

  public $host = 'http://identi.ca/api/';
  /**
   * Set API URLS
   */
  function accessTokenURL()  { return IDENTICA_ACCESS_TOKEN; }
  function authenticateURL() { return IDENTICA_OAUTH_AUTHORIZE; }
  function authorizeURL()    { return IDENTICA_OAUTH_AUTHORIZE; }
  function requestTokenURL() { return IDENTICA_REQUEST_TOKEN; }
  /*function http($url, $method = 'GET', $postfields = NULL) {
    return drupal_http_request($url, array(), $method, $postfields); 
    }*/
}