<?php
// $Id$

/**
 * @file
 */

function identica_profile_settings() {
  $form = array();
  $form['identica_profile_display_type'] = array(
      '#type' => 'select',
      '#title' => t('Track'),
      '#options' => array(
          'account' => t('Account'),
          'group' => t('Group'),
          'hashtag' => t('Hashtag'),
      ),
      '#description' => t('Set the type of object to track'),
      '#default_value' => variable_get('identica_profile_display_type', 'account'),

  );
  $form['identica_profile_display_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Set the name to track'),
      '#default_value' => variable_get('identica_profile_display_name', ''),
  );
  return system_settings_form($form);
}