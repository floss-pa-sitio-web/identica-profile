<?php
// $Id$

require_once 'includes/IdenticaParsers.inc';
define('IDENTICA_URL', 'http://identi.ca/');
define('IDENTICA_GROUP', IDENTICA_URL . 'api/statusnet/groups/timeline/');
define('IDENTICA_USER_TIMELINE', IDENTICA_URL . 'api/statuses/user_timeline/');
define('IDENTICA_TAG', IDENTICA_URL . 'api/statusnet/tags/timeline/');
define('IDENTICA_PUBLIC_TIMELINE', IDENTICA_URL . 'api/statuses/public_timeline.xml');
define('IDENTICA_REQUEST_TOKEN', IDENTICA_URL . 'api/oauth/request_token');
define('IDENTICA_ACCESS_TOKEN', IDENTICA_URL . 'api/oauth/access_token');
define('IDENTICA_OAUTH_AUTHORIZE', IDENTICA_URL . 'api/oauth/authorize');
  
function identica_profile_timeline($public = true) {
  if ($public) {
    $response = drupal_http_request(IDENTICA_PUBLIC_TIMELINE);
  }
  return _identica_profile_parse_response($response);
}

function identica_profile_user_timeline($user, $format = 'xml') {
  $url = IDENTICA_USER_TIMELINE . check_plain($user) . ".{$format}";
  return _identica_profile_parse_response(drupal_http_request($url));
}

function identica_profile_by_hashtag($hashtag) {
  $url = IDENTICA_TAG . check_plain($hashtag) . '.xml';
  return _identica_profile_parse_response(drupal_http_request($url));
}

function identica_profile_by_group($group) {
	$url = IDENTICA_GROUP . check_plain($group) . '.xml';
	return _identica_profile_parse_response(drupal_http_request($url));
}

function _identica_profile_parse_response($response) {
  $posts = array();
  if ($response->code == '200' || $response->code == '304') {
    $xml = simplexml_load_string($response->data);
    foreach ($xml->status as $status) {
      $post = array();
      $post['text'] = (string) _identica_profile_parse_hashtags_users($status->text);
      $post['created_at'] = (string) $status->created_at;
      $post['user'] = (string) $status->user->screen_name;
      $post['profile_image_url'] = (string) $status->user->profile_image_url;
      $posts[] = $post;
    }
  }
  return $posts;
}

function _identica_profile_parse_hashtags_users($text) {
  $options = array(
    'attributes' => array(
      'target' => '_blank',
    )
  );
  //First change usernames to user's identica url
  $text = preg_replace('/[@]+([A-Za-z0-9_]+)/', l('@${1}', IDENTICA_URL . '${1}', $options), $text);
  //change the hashtags
  $text = preg_replace('/[#]+([A-Za-z0-9_\-\.]+)/', l('#${1}', IDENTICA_URL . 'tag/${1}', $options), $text);
  //change the groups
  $text = preg_replace('/[!]+([A-Za-z0-9_\-\.]+)/', l('!${1}', IDENTICA_URL . 'group/${1}', $options), $text);
  return $text;
}
